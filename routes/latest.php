<?php namespace EDMboard;

add_action( 'rest_api_init', function () {
		register_rest_route( 'edm/v1', '/latest', array(
			'methods' => 'POST',
			'callback' => 'EDMboard\RouteActions::get_latest_json',
		));
});
