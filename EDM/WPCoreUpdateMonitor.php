<?php namespace EDM;

class WPCoreUpdateMonitor {
	static public function checkPlugins() {
		$stored = get_site_transient('update_plugins');
		if ($stored && isset($stored->response)) {
			return $stored->response;
		}
		return array();
	}

	static public function getPluginSlugs() {
		return get_transient('plugin_slugs');
	}

	static public function checkCore() {
		$stored = get_site_transient('update_core');
		if ($stored && isset($stored->response)) {
			return $stored->response;
		}
		return array();
	}
}
