<?php namespace EDM;

class Option {
	/*
	type
	name
	display name
	placeholder
	*/

	function __construct($type = '', $name = '', $displayName = '', $placeholder = '', $setting_name) {
		$this->type = $type;
		$this->name = $name;
		$this->displayName = $displayName;
		$this->placeholder = $placeholder;
		switch ($this->type) {
			case 'input_field':
				$this->displayFn = array($this, 'input_field');
				break;	
		}
        register_setting($setting_name, $this->name);
	}

    function input_field($args) {
        $value = get_option($args['id']);
        $type = 'type="text"';
        if (isset($args['hidden'])) {
            $type = 'type="hidden"';
        }
        echo '<input id="' . esc_attr($args['id']) . '" name="' . esc_attr($args['id']) . '" value="' . $value . '" placeholder="' . esc_attr($args['placeholder']) . '" ' . $type . '/>'; 
    }
}


class EnqueuedAsset {
	/*
	$name;
	$link;
	$dependencies;
	$version;
	$isFooter;
	*/

	function __construct($type = '', $name = '', $link = '', $dependencies = '', $version = '', $isFooter = true) {
		$this->type 		= $type;
		$this->name 		= $name;
		$this->link 		= $link;
		$this->dependencies = $dependencies;
		$this->version 		= $version;
		$this->isFooter 	= $isFooter;
	}
}

Class OptionsPage {
    function __construct($settings) {
    	if (empty($settings['settingName'])) {
    		return false;
    	}

        $this->setting_name = $settings['settingName'];

        if (!empty($settings['displayName'])) {
	        $this->setting_display_name = $settings['displayName'];
        } else {
        	$this->setting_display_name = $settings['settingName'];
        }

        if (!empty($settings['enqueues']) && is_array($settings['enqueues'])) {
	        $this->enqueues = $settings['enqueues'];
        } else {
        	$this->enqueues = array();
        }

        if (!empty($settings['options']) && is_array($settings['options'])) {
	        $this->options = $settings['options'];
        } else {
        	$this->options= array();
        }

        add_action("admin_menu", array($this, "action_admin_menu"));
        add_action("admin_init", array($this, "settings_cb"));

    }

    function enqueue_action() { 
        add_action( 'admin_enqueue_scripts', array($this, 'enqueue_assets'));
    }

    function enqueue_assets() {
        foreach($this->enqueues as $asset) {
        	switch($asset->type) {
        		case 'css':
		        	wp_enqueue_style($css->name, $css->link);
        			break;
        		case 'js':
			        wp_enqueue_script($js->name, $js->link, $js->dependencies, $js->isFooter);
        			break;
        	}
        }
    }

    function action_admin_menu() {
        $submenu = add_submenu_page( 'options-general.php', $this->setting_display_name, $this->setting_display_name, 'manage_options', $this->setting_name, array($this, 'html_call_back') ) ;
        add_action('load-' . $submenu, array($this, 'enqueue_action'));
    }

    function settings_cb() {
        $this->_users = get_users();
        add_settings_section($this->setting_name . '_settings', '', array($this, 'create_settings_fields'), $this->setting_name);
    }

    function create_settings_fields() {
        foreach($this->options as $option) {
            add_settings_field($option->name,  $option->displayName, $option->displayFn, $this->setting_name, $this->setting_name . '_settings', ['id' => $option->name, 'placeholder' => $option->placeholder]);
        }
    }

    function get_select_option($value, $name) {
    	return '<option value="' . $value . '">' . $name . '</option>';
    }

    function input_field($args) {
        $value = get_option($args['id']);
        $type = 'type="text"';
        if (isset($args['hidden'])) {
            $type = 'type="hidden"';
        }
        echo '<input id="' . esc_attr($args['id']) . '" name="' . esc_attr($args['id']) . '" value="' . $value . '" placeholder="' . esc_attr($args['placeholder']) . '" ' . $type . '/>'; 
    }

    function html_call_back() {
        if (!current_user_can('manage_options')) return;

        if (isset($_GET['settings-updated'])) {
            add_settings_error($this->setting_name . '_messages', $this->setting_name . '_messages', 'Settings Saved', 'updated');
        }
        ?>
        <div class="wrap" id="<?php echo $this->setting_name;?>-options">
            <h1><?= esc_html(get_admin_page_title()); ?></h1>
            <form action="/wp-admin/options.php" method="post">
                <?php
                    settings_fields($this->setting_name);
                    do_settings_sections($this->setting_name);
                    submit_button('Save');
                ?>
            </form>
        </div>
        <?php
    }
}
