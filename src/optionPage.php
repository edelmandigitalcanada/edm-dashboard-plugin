<?php namespace EDMboard;

/****
*
* EDM Dashboard Api options
*
* "EDMboard_secret" 			= Secret used to authenticate with dashboard during API calls.
* "EDMboard_rate_limit_hours" 	= Minimum time (in hours) that must pass between API calls for this plugin to respond.
* "EDMboard_origin" 			= URL of the dashboard from which the API will be receiving calls. Calls from other URLs will not be answered.
*
*
***********************************************/

$optionPageSettings = array(
	'settingName' 	=>	'EDMboard',
	'displayName'	=>	'EDM Dashboard API',
	/*'enqueues'		= array(
						'css'	= array(EnqueuedAsset('css')),
						'js'	= array(EnqueuedAsset('js')),
					),*/
	'options'		=> array(
							new \EDM\Option('input_field', 'EDMboard_origin', 'Origin/Dashboard URL', '', 'EDMboard'),
							new \EDM\Option('input_field', 'EDMboard_secret', 'Authentication Secret', '', 'EDMboard'),
							new \EDM\Option('input_field', 'EDMboard_rate_limit_hours', 'Ratelimit (in hours)', '0.15', 'EDMboard'),
						)
);

$optionPage = new \EDM\OptionsPage($optionPageSettings);
