<?php namespace EDMboard;

class RateLimiter {
	public static function can_call() {
		if (!get_transient('EDMboard_rate_limit')) {
			return true;
		}
		return false;
	}

	public static function set_limit() {
		$limit = floatval(get_option('EDMboard_rate_limit_hours'));
		if ($limit) { //this check ensures that we don't create a transient with "0" duration
			set_transient('EDMboard_rate_limit', true,  $limit * HOUR_IN_SECONDS);
		}
	}
}
