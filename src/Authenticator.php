<?php namespace EDMboard;

/***
*
Authenticator checks :
	$_SERVER['HTTP_REFERER'] for match with EDMboard_origin option
	$_SERVER['Authorization'] OR $_SERVER['HTTP_AUTHORIZATION'] for "Bearer" match with EDMboard_secret option 
*
*/

class Authenticator {
	public static function isAuthenticated() {
		if (Authenticator::validReferer() && Authenticator::validSecret()) {
			return true;
		}
		return false;
	}


	public static function getAuthorizationHeader(){
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

    public static function getBearerToken() {
	    $headers = Authenticator::getAuthorizationHeader();
	    if (!empty($headers)) {
	        if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
	            return $matches[1];
	        }
	    }
	    return null;
	}

	public static function validSecret() {
		$provided_secret = Authenticator::getBearerToken();
		$valid_secret = get_option('EDMboard_secret');
		if (empty($valid_secret) || ($provided_secret && $provided_secret === $valid_secret)) {
			return true;
		}
		return false;
	}

	public static function validReferer() {
		$referer = get_option('EDMboard_origin');
		if (!empty($referer) && (!isset($_SERVER['HTTP_REFERER']) || $_SERVER['HTTP_REFERER'] !== $referer)) {
			return false;
		}
		return true;
	}
}
