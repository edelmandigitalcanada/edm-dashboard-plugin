<?php namespace EDMboard;

class Version {
	public static function compare_versions($fields) {
		$response = 'Could not determine latest version.';
		if (!empty($fields['latest_version'])) {
			if ($fields['Version'] !== $fields['latest_version']) {
				$response = 'Needs update.';
			} else {
				$response = 'Up-to-date.';
			}
		}
		return $response;
	}
}
