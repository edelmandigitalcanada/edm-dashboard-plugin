<?php namespace EDMboard;

class RouteActions {
	public static function get_latest_json(\WP_REST_Request $request) {
		list($valid, $response) = Authorizator::check();
		if ($valid) {
			$response = array_merge(array('success' => true), Core::get_latest(), Plugins::get_latest());
		}
		return $response;
	}

	public static function get_current_json(\WP_REST_Request $request) {
		list($valid, $response) = Authorizator::check();
		if ($valid) {
			$response = array_merge(array('success' => true), Core::get_current(), Plugins::get_current());
		}
		return $response;
	}
}
