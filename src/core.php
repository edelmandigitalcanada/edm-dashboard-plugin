<?php namespace EDMboard;
/*
*
* Core related functions
*
*/

class Core {
	private $template = array();

	public static function get_current() {
		return Core::buildResponseArray(array("Version" => Core::get_version()));
	}

	public static function get_latest() {
		$properties = array(
						"Version" => Core::get_version(),
					);

		$WPmonitorCoreInfor = \EDM\WPCoreUpdateMonitor::checkCore();

		if (isset($WPmonitorPluginInfo->updates)) {
			$properties['latest_version'] = $WPmonitorPluginInfo->updates->version;
		} else {
			$properties['latest_version'] = Core::fetch_latest_from_api();
		}

		$properties['update_status'] = Version::compare_versions($properties);
		return Core::buildResponseArray($properties);
	}

	static function fetch_latest_from_api() {
		$url = 'https://api.wordpress.org/core/version-check/1.7/';
		$response = wp_remote_get($url);

		if (!is_wp_error($response)) {
			$obj = json_decode($response['body']);
			$upgrade = $obj->offers[0];
			return $upgrade->version;
		}

		// @TODO: response when errors !?!
	}

	static function buildResponseArray($propertiesArray) {
		return array(
				"WP-core" => $propertiesArray,
			);
	}

	static function get_version() {
		return get_bloginfo('version');
	}
}
