<?php namespace EDMboard;
/*
*
* Plugin related functions
*
*/

class Plugins {
	public static function get_current() {
		Plugins::get_requires();
		return get_plugins();
	}

	public static function get_latest() {
		$WPmonitorPluginInfo = \EDM\WPCoreUpdateMonitor::checkPlugins();

			// "new_version"

		Plugins::get_requires();
		$all_plugins = get_plugins();
		foreach ($all_plugins as $plugin => $fields) {
			if (isset($WPmonitorPluginInfo[$plugin])) {
				$fields['latest_version'] = $WPmonitorPluginInfo[$plugin]->new_version;
				$fields['update_status'] = Version::compare_versions($fields);
				$all_plugins[$plugin] = $fields;
			} else {
				$all_plugins[$plugin] = Plugins::get_latest_plugin($plugin, $fields);
			}
		}
		return $all_plugins;
	}

	static function get_plugin_slug($plugin, $fields, $attempt = 0) {
		/*
		Slug identification strategies:
			- basename($plugin, '.php'),
			- sanitize_title($plugin['Name')
			- $plugin['TextDomain']
		*/

		$slug = '';
		
		if ($attempt < 1 && !empty($fields['TextDomain'])) {
			$slug = $fields['TextDomain'];
		}

		if ($attempt < 2 && empty($slug)) {
			$slug = sanitize_title($fields['Name']);
		}

		if ($attempt < 3 && empty($slug)) {
			$slug = basename($plugin, '.php');
		}
		return $slug;
	}

	static function get_latest_plugin_version($plugin, $fields, $attempt = 0) {
		$slug = Plugins::get_plugin_slug($plugin, $fields, $attempt);
		$args = array(
		    'slug' => $slug,
		    'fields' => array(
		        'version' => true,
		    ),
		);

		$version = '';
		$call_api = plugins_api('plugin_information', $args);
		if (is_wp_error($call_api)) {
		    $api_error = $call_api->get_error_message();
		    if ($attempt < 3) {
		    	$attempt++;
	    	 	$version = Plugins::get_latest_plugin_version($plugin, $fields, $attempt);
		    }
		} else if (!empty($call_api->version)) {
	       $version = $call_api->version;
	    }
	    return $version;
	}

	static function get_latest_plugin($plugin, $fields) {
			if (empty($plugin['latest_version'])) {
				$fields['latest_version'] = Plugins::get_latest_plugin_version($plugin, $fields);
			}
			$fields['update_status'] = Version::compare_versions($fields);

			return $fields;
	}

	static function get_requires() {
		if (!function_exists( 'plugins_api')) {
			require_once( ABSPATH . 'wp-admin/includes/plugin-install.php' );
		}

		if (!function_exists('get_plugins')) {
			require_once( ABSPATH . 'wp-admin/includes/plugin.php');
		}
	}
}
