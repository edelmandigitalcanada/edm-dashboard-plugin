<?php namespace EDMboard;

class Authorizator {
	public static function check() {
		$response = '';
		$valid = false;

		if (Authenticator::isAuthenticated()) {
			if (RateLimiter::can_call()) {
				$valid = true;
				RateLimiter::set_limit();
			} else {
				$response = array(
					'success' => false,
					'message'	=> 'Not allowed at this time.'
				);
			}
		} else {
			$response = array(
					'success' => false,
					'message'	=> 'Authentication required.'
				);
		}

		return array($valid, $response);
	}
}
