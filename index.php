<?php namespace EDMboard;
/*
Plugin Name: EDM Dashboard
Description: Configure and sync to the plugin dashboard
Version:     0.0.1a
Author:      Edelman
Text Domain: EDM-dashboard
Domain Path: /languages
*/

require 'EDM/Option.php';
require 'EDM/WPCoreUpdateMonitor.php';

$dir = dirname( __FILE__ );
$files = preg_grep('/^([^.])*/i', scandir($dir . '/src/'));
foreach($files as $file) {
    $path = $dir . '/src/' . $file;
    if (is_dir($path) && !in_array($file, array('..', '.'))) {
        $subFiles = preg_grep('/^([^.])*\.php/i', scandir($path));
        foreach($subFiles as $subFile) {
            require $path . '/' . $subFile;
        }
    } else if (is_file($path)) {
    	require $path;
    }
}

require 'routes/latest.php';
require 'routes/current.php';
